#include "fcgi_stdio.h" /* fcgi library; put it first*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>

/* declarations */

void initialize(void) {

}

void main(void) {
    /* Initialization. */  
      initialize();

      /* Response loop. */
        while (FCGI_Accept() >= 0)   {
            printf("Content-Type: text/plain\r\n");
            printf("\r\n");
            printf("%s", getenv("HTTP_CF_IPCOUNTRY"));
        }
}

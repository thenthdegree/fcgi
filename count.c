#include "fcgi_stdio.h" /* fcgi library; put it first*/

#include <stdlib.h>

int count;

void initialize(void)
{
      count=0;
}

void main(void)
{
    /* Initialization. */  
      initialize();

      /* Response loop. */
        while (FCGI_Accept() >= 0)   {
            count = count+1;
            printf("Content-type: text/plain\r\n");
            printf("\r\n");
            printf("%d", count);
        }
}

#include "fcgi_stdio.h" /* fcgi library; put it first*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>

/* declarations */

int fsize();
void sendImage();
bool checkCookie();
bool checkCountry();
void setCookie();
unsigned int generateRandom(unsigned int min, unsigned int max);
void redirect();

/* vars */
FILE *image;
unsigned int size;
unsigned char *imagebuffer;

int urlnumber = 7;
const char *urls[] = {  "http://www.amazon.com/gp/product/B00M55C1I2/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B00M55C1I2&linkCode=as2&tag=noiblu-20&linkId=ZRVBHOWRAVWDMKS2",
                        "http://www.amazon.com/gp/product/B00GXKTEUI/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B00GXKTEUI&linkCode=as2&tag=noiblu-20&linkId=GH74OOSTLAI4GNJT", 
                        "http://www.amazon.com/gp/product/B00DR0PDNE/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B00DR0PDNE&linkCode=as2&tag=noiblu-20&linkId=ASIJYLFMPLYM4XFX", 
                        "http://www.amazon.com/gp/product/B00HVLUR86/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B00HVLUR86&linkCode=as2&tag=noiblu-20&linkId=UWNNB5I4NK3OQLP3", 
                        "http://www.amazon.com/gp/product/B00GB2YASO/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&linkCode=as2&tag=noiblu-20",
                        "http://www.amazon.com/gp/product/B00IVPU7AO/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B00IVPU7AO&linkCode=as2&tag=noiblu-20&linkId=33BWEJJNKBTNQOSA",
                        "http://www.amazon.com/gp/product/B001F7AJKI/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B001F7AJKI&linkCode=as2&tag=noiblu-20&linkId=HYX2V3UN6X6KVVNY" };

void initialize(void) {
      image = fopen("image.png", "rb");
      size = fsize(image);
      imagebuffer = malloc(size);
      fread(imagebuffer, sizeof(unsigned char), size, image);
}

void main(void) {
    /* Initialization. */  
      initialize();

      /* Response loop. */
        while (FCGI_Accept() >= 0)   {

            setCookie();

            if(!getenv("HTTP_REFERER") || checkCookie()) {
                sendImage(); 
            } else {
                if(generateRandom(1, 10) >= 5) {
                    redirect();
                } else {
                    sendImage();
                }
            }

        } 
}

void redirect() {

    int r = generateRandom(0, urlnumber-1);
    
    printf("Random: %d\r\n", r);
    printf("Status: 302\r\n");
    printf("Location: %s\r\n", urls[r]);
    printf("\r\n");
}

unsigned int generateRandom(unsigned int min, unsigned int max) {
    int r;
    const unsigned int range = max - min;
    const unsigned int buckets = RAND_MAX / range;
    const unsigned int limit = buckets * range;
    
    srand(time(NULL));

    do {
        r = rand();
    } while (r >= limit);

    return min + (r / buckets);
}

bool checkCookie() {

    char *cookies;

    if(!getenv("HTTP_COOKIE")) {
        cookies = "hi";
    } else {
        cookies = getenv("HTTP_COOKIE");
    }
    
    char *visit;
    visit = "Visit";
    
    if(strstr(cookies, visit) != NULL) {
        return true;
    } else {
        return false;
    }

}

void setCookie() {
    printf("Set-Cookie: Visit=true; Domain=.thenthdegree.net.au; Path=/; Max-Age=86400; Version=1\r\n");
}

void sendImage() {
    printf("Content-type: image/png\r\n");
    printf("\r\n");
    fwrite(imagebuffer, sizeof(unsigned char), size, stdout);
}

int fsize(FILE *fp){
    int prev=ftell(fp);
    fseek(fp, 0L, SEEK_END);
    int sz=ftell(fp);
    fseek(fp,prev,SEEK_SET); //go back to where we were
    return sz;
}
